import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from '../../Task';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.css'],
})
export class TaskItemComponent implements OnInit {
  @Input() task!: Task;
  faTimes = faTimes;

  @Output() deleteTask = new EventEmitter();
  @Output() toggleReminder = new EventEmitter();

  onDelete(id: number) {
    this.deleteTask.emit(id);
  }

  onToggle(task: Task) {
    this.toggleReminder.emit(task);
  }

  constructor() {}

  ngOnInit(): void {}
}
